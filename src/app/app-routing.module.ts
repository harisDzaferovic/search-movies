import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ShowcaseMoviesComponent } from './showcase-movies/showcase-movies.component';
import { Top20MoviesComponent } from './top20-movies/top20-movies.component';

const routes: Routes = [
  {path: "", pathMatch: "full", redirectTo: "/home"},
  {path: "home", component: HomeComponent},
  {path: "top-20", component:Top20MoviesComponent},
  {path: "movies/:genre", component: ShowcaseMoviesComponent, runGuardsAndResolvers:'always'},
  {path: "query/:searched", component: ShowcaseMoviesComponent, runGuardsAndResolvers: 'always'},
  {path: "notfound", component:NotFoundComponent},
  {path: "**", component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
