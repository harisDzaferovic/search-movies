import { Component } from '@angular/core';
import { ConnectionService } from 'ngx-connection-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'movie-app';

  status = false;
  isConnected = true;

  constructor(private connectionService:ConnectionService){
    this.connectionService.monitor().subscribe( response => {
      this.isConnected = response.hasNetworkConnection
        if(this.isConnected){
          this.status = false;
        }else {
          this.status = true;
        }
    })
  }
}
