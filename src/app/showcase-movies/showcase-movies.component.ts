import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Movie } from 'src/domain/movie';
import { MoviesService } from 'src/services/movies.service';

@Component({
  selector: 'app-showcase-movies',
  templateUrl: './showcase-movies.component.html',
  styleUrls: ['./showcase-movies.component.css']
})
export class ShowcaseMoviesComponent implements OnInit, OnDestroy {

  navigationSubscription: Subscription;

  constructor(private moviesService:MoviesService, private router:Router) {
    
    //Enabling refreshing page that uses same component but different url
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });

  }

  initialiseInvites() {
      this.movieList = []
      this.page = 1;

      if(this.router.url.startsWith("/movies")){
          this.currentRoute = this.router.url.split('/movies/').join('');
          this.getMovies(this.currentRoute,this.page);
          this.title = this.currentRoute;
    }else if(this.router.url.startsWith('/query')){
          this.currentRoute = this.router.url.split('/query/').join('');
          this.currentRoute = this.currentRoute.split('%20').join(' ');
          this.newQuery(this.currentRoute)
          this.title = this.currentRoute;
    }
  }


  @Input() movieList?:Movie[] = []
  totalPages?:number = 0
  totalResults?:number = 0
  currentRoute: string = ""
  page=1
  loaded = false
  newQuerySearched = false;
  querySearched:string = ""
  title:string = ""

  //List of different ids for movie genres - Used for queries when fething by genre
  genres:Map<string,number> = new Map<string,number>([
    ["horror",27],
    ["comedy",35],
    ["adventure",12],
    ["romance",10749],
    ["action",28],
    ["fantasy",14]
  ])

  ngOnInit(): void {
  
  }

  ngOnDestroy(): void {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  //Function used to fetch movies by genre id
  //Page variable used for pagination
  getMovies(route:string, page:number) {
    this.moviesService.getMoviesByGenre(this.genres.get(route)!,page).subscribe( data => {

        this.movieList = this.movieList?.length == 0 ? data.results : this.movieList?.concat(data.results!!)
        this.totalPages = data.total_pages;
        this.totalResults = data.total_results;
        this.page = this.page+1;
        this.loaded = true
        this.newQuerySearched = false;
    }), (err: any) => console.log("HTTP Error", err)
  }

  /* Function called when we try to load more movies - if new user query is searched
    that means we want to load more movies based on that query and not the genre id. */
  loadMore() {

    this.newQuerySearched ?
    this.getMoviesBasedOnQuery(this.querySearched) :
    this.getMovies(this.currentRoute,this.page);
  
  }

  //Function used to fetch movies by user query
  getMoviesBasedOnQuery(query:string) {
    
    this.moviesService.getMovies(query, this.page).subscribe( data => {
      
        this.movieList = this.movieList?.length == 0 ? data.results : this.movieList?.concat(data.results!!)
        this.totalPages = data.total_pages;
        this.totalResults = data.total_results;
        this.page = this.page+1;
        this.loaded = true
    
    }), (err: any) => console.log("HTTP Error", err)
  }

  //Function that gets triggered whenever user types in new query
  newQuery(query:string) {
      
    this.title = query

    this.movieList = [];
    this.page = 1;
    this.querySearched = query; 
    this.newQuerySearched = true;

    this.getMoviesBasedOnQuery(this.querySearched)
  }
}
