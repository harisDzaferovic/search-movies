import { Component, Input} from '@angular/core';
import { Movie } from 'src/domain/movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent{

  constructor() { }

  @Input() movieList?:Movie[] = []

}
