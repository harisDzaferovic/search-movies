import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/domain/movie';
import { MoviesService } from 'src/services/movies.service';

@Component({
  selector: 'app-top20-movies',
  templateUrl: './top20-movies.component.html',
  styleUrls: ['./top20-movies.component.css']
})
export class Top20MoviesComponent implements OnInit {

  constructor(private moviesService:MoviesService) { }

  totalPages?:Number = 0
  movieList?:Movie[] = []

  ngOnInit(): void {
    this.getTop20Movies();
  }

  getTop20Movies() {
    this.moviesService.getTop20Movies().subscribe( data => {
      this.movieList = data.results;
      this.totalPages = data.total_pages;
    })
  }

}
