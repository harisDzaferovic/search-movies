import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  
  constructor(private formBuilder: FormBuilder, private router:Router) { }

  ngOnInit(): void {
  }
  
  checkoutForm = this.formBuilder.group({
    search: ''
  });

  onSubmit(): void {

    if(this.checkoutForm.get('search')?.value != ""){
      this.router.navigate(['/query', this.checkoutForm.get('search')?.value])
      this.checkoutForm.reset();
    }
  }

}
