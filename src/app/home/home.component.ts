import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/domain/movie'
import { MoviesService } from 'src/services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {
  
  constructor(private movieService:MoviesService){}
  
  ngOnInit(): void {
  }

}