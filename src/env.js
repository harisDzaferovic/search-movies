(function (window) {
    window.__env = window.__env || {};
  
    window.__env.API_KEY = '';
    window.__env.URL_QUERY = 'https://api.themoviedb.org/3/search/movie?api_key='
    window.__env.URL_TOP20 = 'https://api.themoviedb.org/3/movie/popular?api_key='
    window.__env.URL_GENRE = 'https://api.themoviedb.org/3/discover/movie?api_key='

  }(this));