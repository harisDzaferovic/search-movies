export interface Movie {
    original_title?: string,
    overview?: string,
    poster_path?: string,
    release_date?: string,
    vote_average?: number
    id?: number
}