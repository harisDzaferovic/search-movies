import { Movie } from "./movie";

export interface MoviesList {
    results?:Movie[];
    total_pages?:number;
    total_results?:number;
}