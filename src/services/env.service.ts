import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  public API_KEY = '';
  public URL_QUERY = '' 
  public URL_TOP20 = ''
  public URL_GENRE = ''

  constructor() { }
}
