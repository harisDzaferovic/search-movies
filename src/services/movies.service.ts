import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MoviesList } from 'src/domain/movies-list';
import { Observable } from 'rxjs';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})

export class MoviesService {

  API_KEY = this.envService.API_KEY;
  URL_QUERY = this.envService.URL_QUERY + this.API_KEY + "&query="
  URL_TOP20 = this.envService.URL_TOP20 + this.API_KEY + "&language=en-US&page=1"
  URL_GENRE = this.envService.URL_GENRE + this.API_KEY + "&with_genres="

  constructor(private http: HttpClient, private envService: EnvService) { }

  getMovies(query:string, page:number): Observable<MoviesList> {
    return this.http.get(this.URL_QUERY+query+"&page="+page);
  }

  getTop20Movies(): Observable<MoviesList> {
    return this.http.get(this.URL_TOP20)
  }

  getMoviesByGenre(id:number,page:number): Observable<MoviesList> {
    return this.http.get(this.URL_GENRE+id+"&page="+page);
  }
}
